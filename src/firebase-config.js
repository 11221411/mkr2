// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore/lite'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB6hTBbuLA5rcumxDVu5Vei6DJOgr-nbc0",
  authDomain: "modul-vue.firebaseapp.com",
  projectId: "modul-vue",
  storageBucket: "modul-vue.appspot.com",
  messagingSenderId: "548196632943",
  appId: "1:548196632943:web:84b8c2319eb8f337b4009a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig)
const db = getFirestore(app)
export default db