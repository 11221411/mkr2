import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import TimeTable from '../views/TimeTable.vue'
import IndividualItemView from '../views/IndividualItemView';

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {path: '/timetable', name: 'timetable', component: TimeTable},
  {
    path: '/timetable-student/:id',
    name: 'timetable-student',
    component: IndividualItemView,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
